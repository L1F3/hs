package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-04-30T13:48:01.058+0300")
@StaticMetamodel(City.class)
public class City_ {
	public static volatile SingularAttribute<City, Long> id;
	public static volatile SingularAttribute<City, String> address;
	public static volatile SingularAttribute<City, Double> Lat;
	public static volatile SingularAttribute<City, Double> Lng;
	public static volatile SingularAttribute<City, Double> NELat;
	public static volatile SingularAttribute<City, Double> NELng;
	public static volatile SingularAttribute<City, Double> SWLat;
	public static volatile SingularAttribute<City, Double> SWLng;
}
