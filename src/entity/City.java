package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "city")
public class City {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "Lat")
	private double Lat;
	
	@Column(name = "Lng")
	private double Lng;
	
	@Column(name = "NELat")
	private double NELat;
	
	@Column(name = "NELng")
	private double NELng;
	
	@Column(name = "SWLat")
	private double SWLat;
	
	@Column(name = "SWLng")
	private double SWLng;
	
	@Transient
	private double distance;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setLat(double lat) {
		Lat = lat;
	}

	public double getLat() {
		return Lat;
	}

	public double getLng() {
		return Lng;
	}

	public void setLng(double lng) {
		Lng = lng;
	}

	public double getNELat() {
		return NELat;
	}

	public void setNELat(double nELat) {
		NELat = nELat;
	}

	public double getNELng() {
		return NELng;
	}

	public void setNELng(double nELng) {
		NELng = nELng;
	}

	public double getSWLat() {
		return SWLat;
	}

	public void setSWLat(double sWLat) {
		SWLat = sWLat;
	}

	public double getSWLng() {
		return SWLng;
	}

	public void setSWLng(double sWLng) {
		SWLng = sWLng;
	}
}
