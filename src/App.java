import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.persistence.NoResultException;

import entity.City;
import service.CSVReaderService;
import service.CityService;
import service.DBService;
import Main.Config;

public class App {
	public static final void main(String args[]) {
		begin();
	}
	
	public static void begin() {
		System.out.println("Importuoti duombaze pagal CSV: 1");
		System.out.println("Ieskoti duombazeje pagal koordinates: 2");
		String command = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			command = br.readLine();
		} catch (IOException ioe) {
			System.out.println("Klaida nuskaitant koordinates!");
			System.exit(1);
		}
		if(command.equals("1")) {
			importing();
			System.out.println();
		}
		else if(command.equals("2")) {
			readCoords();
			System.out.println();
		}
		else {
			System.out.println();
			begin();
		}
	}

	public static void readCoords() {

		System.out.print("Iveskite koordinates: \n(1) Lat \n(2) Lng\n");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String LatStr = null;
		String LngStr = null;
		Double Lat = null;
		Double Lng = null;
		try {
			LatStr = br.readLine();
			LngStr = br.readLine();
		} catch (IOException ioe) {
			System.out.println("Klaida nuskaitant koordinates!");
			System.exit(1);
		}

		try {
			Lat = Double.parseDouble(LatStr);
			Lng = Double.parseDouble(LngStr);
		} catch(NumberFormatException e) {
			System.out.println("Ivesta neskaitiniu formatu");
		}

		try {
			City city = new CityService().findByCoordinates(Lat, Lng);
			System.out.println(city.getAddress());
		} catch (NoResultException e) {
			System.out.println("Nerasta miestu pagal sias koordinates");
			CityService cityService = new CityService();
			List<City> cityList = cityService
					.findNearestByCoordinates(Lat, Lng);
			for (City city : cityList) {
				System.out
						.println(city.getAddress() + " " + city.getDistance());
			}
		}
	}

	public static void importing() {
		CSVReaderService fileInfo = new CSVReaderService(Config.SOURCE);
		DBService db = new DBService(fileInfo.getList());
		db.DBImportFromList(Config.IMPORT_COUNT);
	}
}
