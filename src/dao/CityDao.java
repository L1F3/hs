package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import entity.City;
import entity.City_;


/*
SELECT *, ( 3959 * acos( cos( radians(39) ) * cos( radians( Lat ) ) * cos( radians( Lng ) - radians(20) ) + sin( radians(39) ) * sin( radians( Lat ) ) ) ) AS distance FROM city ORDER BY distance ASC LIMIT 0, 10
*/
public class CityDao {

	private static final String PERSISTENCE_UNIT_NAME = "hotelston";
	EntityManagerFactory factory = Persistence
			.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

	public EntityManager getEntityManager() {
		return factory.createEntityManager();
	}

	public boolean create(City city) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.persist(city);
		em.getTransaction().commit();
		em.close();
		return true;
	}

	public Long findCountByAddress(City city) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<City> c = qb.createQuery(City.class);
		Root<City> p = c.from(City.class);
		Predicate condition = qb.equal(p.get(City_.address), city.getAddress());
		c.where(condition);
		TypedQuery<City> q = em.createQuery(c);
		List<City> result = q.getResultList();
		em.getTransaction().rollback();
		em.close();
		return new Long(result.size());
	}

	public City findCityByCoords(double Lat, double Lng) throws NoResultException {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<City> c = qb.createQuery(City.class);
		Root<City> p = c.from(City.class);
		Predicate condition = qb.le(p.get(City_.SWLat), Lat);
		Predicate condition2 = qb.ge(p.get(City_.NELat), Lat);
		Predicate condition3 = qb.le(p.get(City_.SWLng), Lng);
		Predicate condition4 = qb.ge(p.get(City_.NELng), Lng);
		c.where(condition, condition2, condition3, condition4);

		TypedQuery<City> q = em.createQuery(c);
		City city = null;
		city = q.setMaxResults(1).getSingleResult();

		em.getTransaction().rollback();
		em.close();
		return city;
	}

	public City findById(int id) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();

		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<City> c = qb.createQuery(City.class);
		Root<City> p = c.from(City.class);
		Predicate condition = qb.equal(p.get(City_.id), id);
		c.where(condition);
		TypedQuery<City> q = em.createQuery(c);
		City city = null;
		city = q.getSingleResult();
		return city;
	}
	
	public List<City> find10NearestCities(double Lat, double Lng) throws NoResultException {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		
		Query query = em.createNativeQuery("SELECT id, ( 6334.4 * acos( cos( radians(?) ) * cos( radians( Lat ) ) * cos( radians( Lng ) - radians(?) ) + sin( radians(?) ) * sin( radians( Lat ) ) ) ) AS distance FROM city ORDER BY distance ASC");
		query.setParameter(1, Lat);
		query.setParameter(2, Lng);
		query.setParameter(3, Lat);
		query.setMaxResults(10);
		
		List<Object[]> dbls = query.getResultList();
		List<City> cities = new ArrayList<>();
		for(Object[] o : dbls) {
			City city = findById((Integer) o[0]);
			city.setDistance((Double) o[1]);
			cities.add(city);
		}

		em.getTransaction().rollback();
		em.close();
		return cities;
	}
}