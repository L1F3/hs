package service;

import java.util.List;

import javax.persistence.NoResultException;

import dao.CityDao;
import entity.City;

public class CityService {
	public boolean create(City city) {
		CityDao cityDao = new CityDao();
		if (cityDao.findCountByAddress(city) == 0) {
			cityDao.create(city);
		}
		return true;
	}
	
	public City findByCoordinates(double Lat, double Lng) throws NoResultException {
		CityDao cityDao = new CityDao();
		
		return cityDao.findCityByCoords(Lat, Lng);
	}

	public List<City> findNearestByCoordinates(double Lat, double Lng) {
		CityDao cityDao = new CityDao();
		
		return cityDao.find10NearestCities(Lat, Lng);
	}
	
}
