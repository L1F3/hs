package service;

import java.util.List;

import entity.City;
import rss.GeocodeResponse;
import Google.Geocoding;
import Google.XMLparser;
import Google.implementation.GeocodingImpl;
import Google.implementation.XMLparserImpl;
import Main.Config;

public class DBService {
	List<List<String>> list = null;

	public DBService(List<List<String>> list) {
		this.list = list;
	}

	public boolean DBImportFromList(Long count) {
		CityService cityService = new CityService();
		for (List<String> country : list) {
			if (count <= 0) {
				break;
			}
			count--;
			Geocoding geo = new GeocodingImpl(Config.apiKey);
			geo.enterCity(country.get(0), country.get(1));

			XMLparser xml = new XMLparserImpl();

			GeocodeResponse geoResp = null;
			geoResp = xml.saveXMLToClass(geo.receiveResult(),
					GeocodeResponse.class);
			City city = new City();

			try {
				assignGeoResponeToCity(geoResp, city);
				cityService.create(city);
			} catch (NullPointerException e) {
				System.out.println("Truksta duomenu: "
						+ geoResp.getResult().get(0).getFormattedAddress());
			}
		}
		return true;
	}

	public void assignGeoResponeToCity(GeocodeResponse geoResp, City city)
			throws NullPointerException {

		city.setAddress(geoResp.getResult().get(0).getFormattedAddress());
		city.setLat(geoResp.getResult().get(0).getGeometry().getLocation()
				.getLat());
		city.setLng(geoResp.getResult().get(0).getGeometry().getLocation()
				.getLng());
		city.setNELat(geoResp.getResult().get(0).getGeometry().getBounds()
				.getNortheast().getLat());
		city.setNELng(geoResp.getResult().get(0).getGeometry().getBounds()
				.getNortheast().getLng());
		city.setSWLat(geoResp.getResult().get(0).getGeometry().getBounds()
				.getSouthwest().getLat());
		city.setSWLng(geoResp.getResult().get(0).getGeometry().getBounds()
				.getSouthwest().getLng());
	}
}
