package service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReaderService {
	private String source;

	public CSVReaderService(String source) {
		this.source = source;
	}

	public List<List<String>> getList() {

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		List<List<String>> list = new ArrayList<>();

		boolean skipFirst = true;

		try {
			br = new BufferedReader(new FileReader(this.source));
			List<String> countryList;
			while ((line = br.readLine()) != null) {
				if (!skipFirst) {
					String[] country = line.split(cvsSplitBy);
					countryList = new ArrayList<String>();
					countryList.add(country[0]);
					countryList.add(country[1]);
					list.add(countryList);
				} else {
					skipFirst = false;
				}

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}
}
