package Google;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import rss.GeocodeResponse;

public interface XMLparser {
	Document parseSourceToXML(InputSource source);
	String saveXMLToFile(Document doc, String destination);
	GeocodeResponse saveXMLToClass(Document doc, Class<?> cls);
}
