package Google;

import org.w3c.dom.Document;

public interface Geocoding {
	
	boolean enterCity(String city, String country);
	Document receiveResult();
}
