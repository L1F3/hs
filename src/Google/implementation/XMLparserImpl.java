package Google.implementation;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXB;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import rss.GeocodeResponse;
import Google.XMLparser;

public class XMLparserImpl implements XMLparser {

	@Override
	public Document parseSourceToXML(InputSource source) {
		Document receivedSource = null;
		try {
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			receivedSource = docBuilder.parse(source);
		} catch (SAXException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return receivedSource;
	}

	@Override
	public GeocodeResponse saveXMLToClass(Document doc, Class<?> cls) {
		return (GeocodeResponse) JAXB.unmarshal(new DOMSource(doc), cls);
	}
	
	@Override
	public String saveXMLToFile(Document doc, String destination) {
		
		Transformer transformer;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			Result output = new StreamResult(new File(destination));
			Source input = new DOMSource(doc);
			
			transformer.transform(input, output);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return destination;
	}
	
}
