package Google.implementation;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import Google.Geocoding;

public class GeocodingImpl implements Geocoding {

	private static final String GEOCODING_REQUEST_XML = "https://maps.googleapis.com/maps/api/geocode/xml";
	String apiKey;
	String city;
	String country;

	public GeocodingImpl(String apiKey) {
		this.apiKey = apiKey;
	}

	private URL formUrl() throws MalformedURLException,
			UnsupportedEncodingException {
		URL url = new URL(GEOCODING_REQUEST_XML + "?address="
				+ URLEncoder.encode(city + ", " + country, "UTF-8") + "&key=" + apiKey);
		return url;
	}

	@Override
	public boolean enterCity(String city, String country) {
		this.city = city;
		this.country = country;
		return false;
	}

	public Document receiveResult() {
		Document doc = null;
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.parse(formUrl().openStream());

		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return doc;
	}
}
